Rails.application.routes.draw do
  devise_for :users
  root to: 'projects#index'

  resources :projects do
    resources :tasks do
      resources :comments
      member do
        get :done
      end
    end
    resources :comments
  end
end
