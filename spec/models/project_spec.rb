require 'rails_helper'

RSpec.describe Project, type: :model do
  let(:user) do
    User.create(email: 'admin@example.com',
                password: 'admin@example.com')
  end

  subject do
    Project.new(name: 'My project',
                kind: :work,
                start_date: Time.now,
                end_date: Time.now + 1.week,
                user: user)
  end

  it 'is valid with valid attributes' do
    expect(subject).to be_valid
  end

  it 'is not valid without a name' do
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it 'is not valid with start date greather than end date' do
    subject.start_date = Time.now + 1.month
    expect(subject).to_not be_valid
  end

  it "is not valid if it's overlapped with other project with same kind" do
    Project.create(name: 'Other project',
                   kind: :work,
                   start_date: Time.now - 1.week,
                   end_date: Time.now + 1.day,
                   user: user)
    expect(subject).to_not be_valid
  end
end
