require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:user) { User.create(email: 'admin@example.com') }
  let(:project) do
    Project.new(name: 'My project',
                kind: :work,
                start_date: Time.now,
                end_date: Time.now + 1.week,
                user: user)
  end

  subject do
    Task.new(title: 'My task',
             priority: :high,
             deadline: Time.now + 1.week,
             user: user,
             project: project)
  end

  it 'is not valid without a project' do
    subject.project = nil
    expect(subject).to_not be_valid
  end

  it 'is valid with valid attributes' do
    expect(subject).to be_valid
  end

  it 'is not valid without a title' do
    subject.title = nil
    expect(subject).to_not be_valid
  end

  it 'is not valid without a priority' do
    subject.priority = nil
    expect(subject).to_not be_valid
  end

  it 'is not valid without a deadline' do
    subject.deadline = nil
    expect(subject).to_not be_valid
  end

  it 'task setted as done returns done status' do
    expect(subject.status).to eq :pending
    subject.set_as_done
    expect(subject.status).to eq :done
  end
end
