# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Project management', type: :feature do
  it 'should create a new project', js: true do
    user = create(:user)
    login_as user

    visit projects_path
    click_link 'New project'

    fill_in 'project[name]', with: 'Ruby on Rails'
    select('work', from: 'project[kind]')
    fill_in 'project[start_date]', with: I18n.l(Date.today, format: '%Y-%m-%d')
    fill_in 'project[end_date]', with: I18n.l(Date.today, format: '%Y-%m-%d')

    expect { click_button 'Create Project' }.to change(Project, :count).by(1)

    page.should have_content('Ruby on Rails')
  end
end
