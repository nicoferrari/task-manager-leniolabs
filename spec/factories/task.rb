FactoryBot.define do
  factory :task do
    project
    title { "my task" }
    priority { 'high' }
    deadline { Time.now }
  end
end
