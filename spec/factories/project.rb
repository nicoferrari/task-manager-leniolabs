FactoryBot.define do
  factory :project do
    user
    sequence(:name) { |n| "my project #{n}" }
    kind { 'work' }
    sequence(:start_date) { |n| Time.now + n.day  }
    sequence(:end_date) { |n| Time.now + (n+1).day }
  end
end
