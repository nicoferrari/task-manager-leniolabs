# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TasksController do
  let(:user) { create(:user) }
  let(:project) { create(:project, user: user) }
  let(:task) { create(:task, project: project) }

  before do
    @request.env['devise.mapping'] = Devise.mappings[:user]
    sign_in(user)
  end

  describe 'tasks#create' do
    context 'when task is invalid' do
      it 'render new' do
        task_params = attributes_for(:task)
        task_params[:title] = nil
        post :create, params: { task: task_params, project_id: project.id }
        expect(response).to render_template :new
      end
      it 'should not create task' do
        task_params = attributes_for(:task)
        task_params[:title] = nil
        expect { post :create, params: { task: task_params, project_id: project.id } }.to change(Task, :count).by(0)
      end
    end

    context 'when task is valid' do
      it 'redirect to project tasks index' do
        task_params = attributes_for(:task)
        expect { post :create, params: { task: task_params, project_id: project.id } }.to change(Task, :count).by(1)
      end
      it 'creates task' do
        task_params = attributes_for(:task)
        expect(post(:create, params: { task: task_params, project_id: project.id })).to redirect_to project_tasks_path
      end
    end
  end

  describe 'tasks#index' do
    it 'returns a successful response' do
      get :index, params: { project_id: project.id }
      expect(response).to be_successful
      expect(response).to render_template :index
    end

    it 'assigns @users' do
      get :index, params: { project_id: project.id }
      expect(assigns(:tasks)).to eq([task])
    end
  end
end
