class TasksController < ApplicationController
  before_action :set_project
  before_action :set_task, only: [:edit, :update, :destroy, :done]

  def index
    @tasks = @project.tasks.includes(:user)
  end

  def new
    @task = @project.tasks.build
  end

  def create
    @task = @project.tasks.build(task_params)

    if @task.save
      redirect_to project_tasks_path(@project)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @task.update(task_params)
      redirect_to project_tasks_path(@project)
    else
      render :edit
    end
  end

  def destroy
    @task.destroy
    redirect_to project_tasks_path(@project)
  end

  def done
    @task.set_as_done
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def set_project
    @project = Project.find(params[:project_id])
  end

  def task_params
    params.require(:task).permit(:title,
                                 :description,
                                 :user_id,
                                 :project_id,
                                 :priority,
                                 :deadline)
  end
end
