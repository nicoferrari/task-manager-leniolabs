class CommentsController < ApplicationController
  before_action :set_project
  before_action :set_task, only: [:index, :new, :create, :destroy]

  def index
    @commetable = @task.present? ? @task : @project
    @comments = @commetable.comments
  end

  def new
    if @task.present?
      @comment = @task.comments.build
    else
      @comment = @project.comments.build
    end
  end

  def create
    if @task.present?
      @comment = @task.comments.build(comment_params)
    else
      @comment = @project.comments.build(comment_params)
    end
    if @comment.save
      if @task.present?
        redirect_to project_task_comments_path(@project,@task)
      else
        redirect_to project_comments_path(@project)
      end
    else
      render :new
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    if @task.present?
      redirect_to project_task_comments_path(@project,@task)
    else
      redirect_to project_comments_path(@project)
    end
  end

  private

  def set_task
    @task = Task.find(params[:task_id]) if params[:task_id].present?
  end

  def set_project
    @project = Project.find(params[:project_id])
  end

  def comment_params
    params.require(:comment).permit(:content, :file)
  end
end
