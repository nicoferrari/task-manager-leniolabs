class Task < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :project
  has_many :comments, as: :commentable

  validates_presence_of :title, :priority, :deadline

  enum priority: {
    low:     0,
    medium: 1,
    high:   2
  }

  def status
    done? ? :done : :pending
  end

  def set_as_done
    update_attribute(:done, true)
  end

  def to_s
    title
  end
end
