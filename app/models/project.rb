# frozen_string_literal: true

class Project < ApplicationRecord
  belongs_to :user
  has_many :tasks, dependent: :destroy
  has_many :comments, as: :commentable

  accepts_nested_attributes_for :tasks

  validates_presence_of :name, :kind, :start_date, :end_date
  validate :end_date_grather_than_start_date, if: proc { |p| p.start_date.present? && p.end_date.present? }
  validate :not_overlapping_by_kind

  scope :by_kind, ->(kind) { where(kind: kind) }

  enum kind: {
    work: 0,
    personal: 1,
    family: 2
  }

  def to_s
    name
  end

  private

  def end_date_grather_than_start_date
    errors.add(:end_date, 'End date should be greather than start date') if end_date < start_date
  end

  def same_kind_projects
    user.projects.by_kind(kind).where.not(id: id)
  end

  def not_overlapping_by_kind
    if same_kind_projects.where('end_date > ? and start_date < ?', start_date, end_date).any?
      errors.add(:end_date, 'The date is overlapped with another project')
      errors.add(:start_date, 'The date is overlapped with another project')
    end
  end
end
