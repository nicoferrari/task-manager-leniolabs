module CommentsHelper
  def comment_form_url
    if @comment.commentable.class.name == 'Task'
      [@comment.commentable.project, @comment.commentable, @comment]
    else
      [@comment.commentable, @comment]
    end
  end

  def comment_destroy_path(comment)
    if @task.present?
      project_task_comment_path(@project, @task, comment)
    else
      project_comment_path(@project, comment)
    end
  end

  def new_comment_path_helper
    if @task.present?
      new_project_task_comment_path
    else
      new_project_comment_path
    end
  end

  def back_path_helper
    if @task.present?
      project_tasks_path
    else
      projects_path
    end
  end

  def new_back_path_helper
    if @task.present?
      project_task_comments_path
    else
      project_comments_path
    end
  end
end
